from django import forms
from . import models

class StatusForm(forms.ModelForm):
	status = forms.CharField(widget=forms.Textarea(attrs={
		"required" : True,
		"placeholder" : "What's happening?",
		}))

	class Meta:
		model = models.Status
		fields = ["status"]
			