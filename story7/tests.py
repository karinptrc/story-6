from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from . import views
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

class StoryTujuh(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/story7')
		self.assertEqual(response.status_code, 200)

	def test_using_correct_func(self):
		found = resolve('/story7')
		self.assertEqual(found.func, views.home)

	def test_model_is_success(self):
		new_status = Status.objects.create(status='Happy!', tanggal=timezone.now())
		count_status = Status.objects.all().count()
		self.assertEqual(count_status, 1)

	def test_form_is_post(self):
		response = Client().post('/story7', data={
			'status' : 'Hello!'
			})
		self.assertEqual(response.status_code, 302)

	def test_is_valid(self):
		response = StatusForm(data={
			'status' : 'Hello!'
			})
		self.assertTrue(response.is_valid())

class Story7Functional(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(Story7Functional, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(Story7Functional, self).tearDown()

	def test_status_is_post(self):
		self.browser.get(self.live_server_url)
		idstatus = self.browser.find_element_by_id('id_status')
		post = self.browser.find_element_by_name('Post')

		idstatus.send_keys('Capek (-_-)')
		post.click()
		time.sleep(5)