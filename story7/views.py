from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status

# Create your views here.

def home(request):
	if request.method == 'POST':
		statform = StatusForm(request.POST)
		if statform.is_valid():
			statform.save()
			return redirect('story7:home')
	else:
		statform = StatusForm()

	stats = Status.objects.order_by("tanggal")
	listStatus = {
		'form' : statform,
		'status' : stats,
	}
	return render(request, 'homeStory7.html', listStatus)