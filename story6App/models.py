from django.db import models
from django.utils import timezone

# Create your models here.

class Status(models.Model):
	status = models.CharField(max_length=300)
	tanggal = models.DateTimeField(default=timezone.now)