from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status

# Create your views here.

def landing(request):
	if request.method == 'POST':
		statform = StatusForm(request.POST)
		if statform.is_valid():
			statform.save()
			return redirect('story6App:landingPage')
	else:
		statform = StatusForm()

	stats = Status.objects.order_by("tanggal")
	listStatus = {
		'form' : statform,
		'status' : stats,
	}
	return render(request, 'landing.html', listStatus)