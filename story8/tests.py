from django.test import TestCase, Client
from django.urls import resolve
from . import views

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.
class StoryLapan(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/story8')
		self.assertEqual(response.status_code, 200)

	def test_using_correct_func(self):
		found = resolve('/story8')
		self.assertEqual(found.func, views.home)

	def test_using_right_staticfiles(self):
		response = Client().get('/story8')
		self.assertContains(response, 'static/style8')
		self.assertContains(response, 'static/story8')

class Story8Functional(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(Story8Functional, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(Story8Functional, self).tearDown()

	def test_success_append_div(self):
		self.browser.get('http://localhost:8000/story8')
		time.sleep(1)
		searchBox = self.browser.find_element_by_id('search')
		time.sleep(3)

		searchBox.send_keys('Books', Keys.ENTER)
		time.sleep(2)

		result = self.browser.find_elements_by_id('item')

		status = 0
		if(len(result) != 0):
			status = 200
		else:
			status = 404

		self.assertEqual(status, 200)
