$.ajax({
    url: "https://www.googleapis.com/books/v1/volumes?q=keys",
    success: function(result){
        let book = $('#books');
        book.empty();
        const booksList = result.items;
        for(var i = 0; i< booksList.length ; i++){
            book.append(
                "<div class='col-lg-4 col-sm-6 mb-4' id='item'>" + 
                "<div class='card h-100' style='padding-left:30px'>" + 
                "<img class='card-img-top' src="+ booksList[i].volumeInfo.imageLinks.thumbnail + ">" +
                "<div class='card-body'>" + "<a href=" + booksList[i].volumeInfo.infoLink + ">" +
                "<h5 class='card-title'>" + booksList[i].volumeInfo.title  + 
                "</h5>" + "</a>" +
                "<h6> Penulis: "+ booksList[i].volumeInfo.authors + "</h6>" +"</div>" + 
                "</div>" + 
                "</div>"
            );
        }
    }
});

function search(title){
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + title,
        success: function(result){
            let book = $('#books');
            book.empty();
            const booksList = result.items;
            for(var i = 0; i< booksList.length ; i++){
                book.append(
                    "<div class='col-lg-4 col-sm-6 mb-4' id='item'>" + 
                    "<div class='card h-100' style='padding-left:30px'>" + 
                    "<img class='card-img-top' src="+ booksList[i].volumeInfo.imageLinks.thumbnail + ">" +
                    "<div class='card-body'>" + "<a href=" + booksList[i].volumeInfo.infoLink + ">" +
                    "<h5 class='card-title'>" + booksList[i].volumeInfo.title  + 
                    "</h5>" + "</a>" +
                    "<h6> Penulis: "+ booksList[i].volumeInfo.authors + "</h6>" +"</div>" + 
                    "</div>" + 
                    "</div>"
                );
            }
        }
    });
}

$('#search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search(event.target.value);
    }
});