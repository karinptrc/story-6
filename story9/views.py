from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as login_auth, logout as logout_auth

def index(request):
    user = request.user
    if user.is_authenticated:
        return render(request, 'story9.html')        
    else:
        return redirect('story9:login')

def login(request):
    user = request.user
    login_form = AuthenticationForm()
    
    if user.is_authenticated:
        return redirect('story9:index')

    if request.method == "POST":
        login_form = AuthenticationForm(data = request.POST)
        if login_form.is_valid():
            user = login_form.get_user()
            login_auth(request, user)
            return redirect('story9:index')
    return render(request, 'login.html', {'form':login_form})

def logout(request):
    if request.method == 'POST':
        logout_auth(request)
    return redirect('story9:index')
