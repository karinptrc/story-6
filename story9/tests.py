from django.test import TestCase
from django.urls import resolve
from . import views

# Create your tests here.
class Unit9(TestCase):

        #test url '/lab9' exist or not
    def test_url_exist(self):
        response = self.client.get('/story9/login')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story9/logout')
        self.assertEqual(response.status_code, 302)

        #test url using status view function
    def test_calling_right_views_function(self):
        found = resolve('/story9/login')
        self.assertEqual(found.func, views.login)
        found = resolve('/story9/logout')
        self.assertEqual(found.func, views.logout)

        #test '/lab9/' using templates lab8.html
    def test_using_right_template(self):
        response = self.client.get('/story9/login')
        self.assertTemplateUsed(response, 'login.html')